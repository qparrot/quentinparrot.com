const http = require('http');
const fs = require('fs');
const con = require('./DBConnection');
const express = require('express');
const hostname ='127.0.0.1';
const port = '8081';


const app = express();

app.get( '/', (req,res) => {
		res.writeHead(200, {"Content-Type": "text/html"});
		fs.createReadStream('./index.html').pipe(res);

		var conn = con.getConnection();

		conn.query('SELECT * FROM comments.comments', function(error, results, fields)
			{
				if(error)
					throw error;
				results.forEach((comment) => {
					console.log(comment);
				});
			});
			conn.end();
	});
app.get('/style.css', (req, res) => {
		res.writeHead(200, {'Content-Type': 'text/css'});
		fs.createReadStream('./style.css').pipe(res);
	});
app.get('/header.png', (req, res) => {
		res.writeHead(200, {'Content-Type': 'image/png'});
		fs.createReadStream('./images/header.png').pipe(res);
	});
app.get('/footer.png', (req, res) => {
		res.writeHead(200, {'Content-Type': 'image/png'});
		fs.createReadStream('./images/footer.png').pipe(res);
	});
app.get( '/functions.js', (req, res) => {
		res.writeHead(200, {'Content-Type': 'text/javascript'});
		fs.createReadStream('./functions.js').pipe(res);
	});
app.get('/home', (req, res) => 	{
		res.writeHead(200, {'Content-Type': 'application/json'});
		var conn = con.getConnection();

		conn.query('SELECT * FROM comments.comments', function(error, results, fields)
		{
			if(error)
				throw error;
			var comments = JSON.stringify(results);
			
			res.end(comments);
		});
	});
app.post('/insert', (req, res) => {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		var content = '';
		req.on('data', function(data) {
			content += data;
			var obj = JSON.parse(content);
			console.log("The userName is:" + obj.name);
			console.log("The commentBody is:" + obj.commentBody);

			var conn = con.getConnection();
			conn.query("INSERT INTO comments.comments (comments.userName, comments.date, comments.comment) VALUES (?, ?, ?)", 
				[obj.name, new Date().toISOString(), obj.commentBody], function(error, results, fields)
				{
					if (error)
						throw error;
					console.log("Success!");
				});
			conn.end();
			res.end("Success!");
		});
	});

app.listen(port, hostname, () => {
	console.log(`Server running at http://${hostname}:${port}`);
});
