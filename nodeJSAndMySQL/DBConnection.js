var mysql = require('mysql');

function getConnection()
{
	var con = mysql.createConnection({
		host: 'localhost',
		user: 'root',
		password: 'Jesuissurmysql55.',
		database: 'comments'
	});
	return con;
}

module.exports.getConnection = getConnection;
