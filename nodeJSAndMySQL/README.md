# HeaderAndFooterAsImage

## Goal

Add the backend server that show the previous comments and allows the addition of new comments:

![image](./headerAndFooterAsimage.png "example")

## Problems

- I add a lot of difficulty to learn mysql.
- I couldn't use phpmyadmin from lampp, so I learn the basic of command line for mysql:
	- USE database;
	- SHOW ALL DATABASES;
	- DESCRIBE table;
	- INSERT INTO (..., ...) VALUES (?, ?);
	- DROP DATABASE database;
	- SELECT * FROM database.table;


## Ressources

- https://www.youtube.com/watch?v=SmgpRNyR3bo

