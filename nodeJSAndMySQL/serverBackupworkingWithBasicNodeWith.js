const http = require('http');
const fs = require('fs');
const con = require('./DBConnection');
const express = require('express');
const hostname ='127.0.0.1';
const port = '8081';

const app = express();

app.get('/', (req, res) => {
		res.send('coucou');
	}
);

app.listen(port, hostname, () => {
	console.log(`Server running at http://${hostname}:${port}`);
});
