const http = require('http');
const fs = require('fs');
const nodemailer = require('nodemailer');
const { google } = require ('googleapis');
const hostname = '127.0.0.1';
const port = '8082';
const url = require('url');




const myEmailTransporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: '465',
	secure: true,
	auth: {
		type: 'OAuth2',
		user: 'parrot.quentin@gmail.com',
		clientId:"976935493782-mdpb867hgutk4s5n0gdec7b9pnf4hitj.apps.googleusercontent.com", 
		clientSecret: "UDbu_E8WsvMkV95OgO07yX8e",
		accessToken: 'ya29.Il-6B7dCuXS2T_sKYpBmXU8grtiToeF6L27cJZbEc4s8UetQGnIaWSpXR9ftUNmG-bz2QM7wko6vlCcyzGqQxklesTZIMWiGPaIUU8-XaMj853PMFIbjhhpJLmNu2b6A8g',
		refreshToken: '1//09_aKl6ePlh8CCgYIARAAGAkSNwF-L9IrR4djaYTHmG2u2DvTN12kKr7DEZoz8bJh6hJ2jlMtdxpVUrG6NET2cfsRkV-_aIoyIpE',
		expires: Number.parseInt(1579609137015, 10)
	}
});

const server = http.createServer((req, res) => {
	var path = url.parse(req.url).pathname;
	var fsCallback = function(error, data) {
		if(error)
			throw error;
		res.writeHead(200);
		res.write(data);
		res.end();
	}

	if (req.method === 'GET' && req.url === '/')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		fs.createReadStream('./index.html').pipe(res);

	}
	if (req.method === 'GET' && req.url ==='/beautybackground.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./beautybackground.js').pipe(res);
	}
	if (req.method === 'GET' && req.url ==='/index.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./index.js').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/beautybackground.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./beautybackground.js').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/style2.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		fs.createReadStream('./style2.css').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/style.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		fs.createReadStream('./style.css').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/img/portfolio.png')
	{
		res.writeHead(200, { 'Content-Type': 'img/png'});
		fs.createReadStream('./img/portfolio.png').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/img/threeBackground.png')
	{
		res.writeHead(200, { 'Content-Type': 'img/png'});
		fs.createReadStream('./img/threeBackground.png').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/img/favicon.png')
	{
		res.writeHead(200, { 'Content-Type': 'img/png'});
		fs.createReadStream('./img/favicon.png').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/CV_Parrot-Quentin.pdf')
	{
		res.writeHead(200, { 'Content-Type': 'application/pdf'});
		fs.createReadStream('./CV_Parrot-Quentin.pdf').pipe(res);
	}

	/* Data Visualisation */
	if (req.method === 'GET' && req.url === '/data-visualization/video-game-sales-data.json')
	{
		res.writeHead(200, { 'Content-Type': 'text/json'});
		fs.createReadStream('./data-visualization/video-game-sales-data.json').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/data-visualization/cities.csv')
	{
		res.writeHead(200, { 'Content-Type': 'text/csv'});
		fs.createReadStream('./data-visualization/cities.csv').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/data-visualization/counties.geojson')
	{
		res.writeHead(200, { 'Content-Type': 'text/geojson'});
		fs.createReadStream('./data-visualization/counties.geojson').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/data-visualization/functions.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./data-visualization/functions.js').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/data-visualization/choroplethMap.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./data-visualization/choroplethMap.js').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/data-visualization/scatterplotGraph.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./data-visualization/scatterplotGraph.js').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/data-visualization/treemapDiagram.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./data-visualization/treemapDiagram.js').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/data-visualization/heatMap.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./data-visualization/heatMap.js').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/data-visualization/barChart.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./data-visualization/barChart.js').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/data-visualization/style.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		fs.createReadStream('./data-visualization/style.css').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/data-visualization/index.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		fs.createReadStream('./data-visualization/index.html').pipe(res);
	}


	/* THOMAS PROJECT */ 

	if (req.url === '/thomas-project/index.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/index.html',fsCallback);
	}
	if (req.url === '/thomas-project/format-adultes.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/format-adultes.html',fsCallback);
	}
	
	if (req.url === '/thomas-project/index.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/index.js',fsCallback);
	}
	if (req.url === '/thomas-project/style.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/style.css',fsCallback);
	}
	if (req.url === '/thomas-project/style-format.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/style-format.css',fsCallback);
	}
	if (req.url === '/thomas-project/imgPresentationFormats1.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/imgPresentationFormats1.jpeg',fsCallback);
	}
	if (req.url === '/thomas-project/expertise.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/expertise.html',fsCallback);
	}
	if (req.url === '/thomas-project/vision.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/vision.html',fsCallback);
	}
	
	if (req.url === '/thomas-project/format-lyceens.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/format-lyceens.html',fsCallback);
	}
	if (req.url === '/thomas-project/format-etudiants.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/format-etudiants.html',fsCallback);
	}
	if (req.url === '/thomas-project/format-colleges.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/format-colleges.html',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/showcase.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/showcase.jpeg',fsCallback);
	}

	/* TEAM TEST */


	if (req.url === '/thomas-project/testTeam/index.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testTeam/index.js',fsCallback);
	}
	if (req.url === '/thomas-project/testTeam/style.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testTeam/style.css',fsCallback);
	}
	if (req.url === '/thomas-project/testTeam/index.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testTeam/index.html',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testTeam/portrait1.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testTeam/portrait1.jpeg',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testTeam/portrait2.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testTeam/portrait2.jpeg',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testTeam/portrait3.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testTeam/portrait3.jpeg',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testTeam/portrait4.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testTeam/portrait4.jpeg',fsCallback);
	}
	/* FOOTER TEST */


	if (req.url === '/thomas-project/testFooter/responsive.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testFooter/responsive.css',fsCallback);
	}
	if (req.url === '/thomas-project/testFooter/style.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testFooter/style.css',fsCallback);
	}
	if (req.url === '/thomas-project/testFooter/index.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testFooter/index.html',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testFooter/la_ruche.gif')
	{
		res.writeHead(200, { 'Content-Type': 'img/gif'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testFooter/la_ruche.gif',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testFooter/le_social_bar.png')
	{
		res.writeHead(200, { 'Content-Type': 'img/png'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testFooter/le_social_bar.png',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testFooter/cojob.png')
	{
		res.writeHead(200, { 'Content-Type': 'img/png'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testFooter/cojob.png',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testFooter/bsb.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testFooter/bsb.jpeg',fsCallback);
	}

	/* HOMEPAGE TEST */


	if (req.url === '/thomas-project/testHomepage/style.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testHomepage/style.css',fsCallback);
	}
	if (req.url === '/thomas-project/testHomepage/index.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testHomepage/index.html',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testHomepage/la_ruche.gif')
	{
		res.writeHead(200, { 'Content-Type': 'img/gif'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testHomepage/la_ruche.gif',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testHomepage/le_social_bar.png')
	{
		res.writeHead(200, { 'Content-Type': 'img/png'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testHomepage/le_social_bar.png',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testHomepage/cojob.png')
	{
		res.writeHead(200, { 'Content-Type': 'img/png'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testHomepage/cojob.png',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testHomepage/bsb200.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testHomepage/bsb200.jpeg',fsCallback);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testHomepage/index.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('/var/www/quentinparrot.com/html/thomas-project/testHomepage/index.js').pipe(res);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testHomepage/jquery.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('/var/www/quentinparrot.com/html/thomas-project/testHomepage/jquery.js').pipe(res);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testHomepage/formation1.jpg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpg'});
		fs.createReadStream('/var/www/quentinparrot.com/html/thomas-project/testHomepage/formation1.jpg').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testHomepage/Ansan_suki_1920x1080.png')
	{
		res.writeHead(200, { 'Content-Type': 'img/png'});
		fs.createReadStream('/var/www/quentinparrot.com/html/thomas-project/testHomepage/Ansan_suki_1920x1080.png').pipe(res);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testHomepage/portrait1.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testHomepage/portrait1.jpeg',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testHomepage/portrait2.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testHomepage/portrait2.jpeg',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testHomepage/portrait3.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testHomepage/portrait3.jpeg',fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testHomepage/portrait4.jpeg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpeg'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testHomepage/portrait4.jpeg',fsCallback);
	}

	/* NAV BAR TEST */


	if (req.url === '/thomas-project/testNavBar/style.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testNavBar/style.css',fsCallback);
	}
	if (req.url === '/thomas-project/testNavBar/index.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testNavBar/index.html',fsCallback);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testNavBar/index.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./thomas-project/testNavBar/index.js').pipe(res);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testNavBar/jquery.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./thomas-project/testNavBar/jquery.js').pipe(res);
	}

	/* FORMATIONS TEST */


	if (req.url === '/thomas-project/testFormations/style.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testFormations/style.css',fsCallback);
	}
	if (req.url === '/thomas-project/testFormations/index.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testFormations/index.html',fsCallback);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testFormations/index.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./thomas-project/testFormations/index.js').pipe(res);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testFormations/jquery.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./thomas-project/testFormations/jquery.js').pipe(res);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testFormations/formation1.jpg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpg'});
		fs.createReadStream('./thomas-project/testFormations/formation1.jpg').pipe(res);
	}
	
	/* SHOWCASE 1000px TEST */


	if (req.url === '/thomas-project/testShowcase/1000px/style.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testShowcase/1000px/style.css',fsCallback);
	}
	if (req.url === '/thomas-project/testShowcase/1000px/index.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testShowcase/1000px/index.html',fsCallback);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testShowcase/1000px/index.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./thomas-project/testShowcase/1000px/index.js').pipe(res);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testShowcase/1000px/jquery.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		fs.createReadStream('./thomas-project/testShowcase/1000px/jquery.js').pipe(res);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testShowcase/1000px/Ansan_suki_1024x575.jpg')
	{
		res.writeHead(200, { 'Content-Type': 'img/jpg'});
		fs.createReadStream('./thomas-project/testShowcase/1000px/Ansan_suki_1024x575.jpg').pipe(res);
	}

	/* SHOWCASE 1920px TEST */


	if (req.url === '/thomas-project/testShowcase/1920px/style.css')
	{
		res.writeHead(200, { 'Content-Type': 'text/css'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testShowcase/1920px/style.css',fsCallback);
	}
	if (req.url === '/thomas-project/testShowcase/1920px/index.html')
	{
		res.writeHead(200, { 'Content-Type': 'text/html'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testShowcase/1920px/index.html',fsCallback);
	}
	if (req.method === 'GET' && req.url ==='/thomas-project/testShowcase/1920px/index.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testShowcase/1920px/index.js', fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testShowcase/1920px/jquery.js')
	{
		res.writeHead(200, { 'Content-Type': 'text/javascript'});
		doc = fs.readFile('/var/www/quentinparrot.com/html/thomas-project/testShowcase/1920px/jquery.js', fsCallback);
	}
	if (req.method === 'GET' && req.url === '/thomas-project/testShowcase/1920px/Ansan_suki_1920x1080.png')
	{
		res.writeHead(200, { 'Content-Type': 'img/png'});
		fs.createReadStream('/var/www/quentinparrot.com/html/thomas-project/testShowcase/1920px/Ansan_suki_1920x1080.png').pipe(res);
	}
	/* CONTACT FORM */ 
	if (req.method === 'POST' && req.url === '/sendForm')
	{
		res.writeHead(200, {'Content-Type': 'text/plain'});
		var content = '';
		req.on('data', function(data) {
			content += data;
			content = contentCleaner(content);
			if(content.firstName && content.lastName && content.email && content.subject && content.body)
			{
				var mailOptions = {
					from: "mail@sds.com",
					to: 'parrot.quentin@gmail.com',
					subject: `NEW EMAIL FROM PORTFOLIO`,
					text: `You received one message from ${content.firstName} ${content.lastName}.\n\nThe email address is ${content.email}.\n\nObject: ${content.subject}.\n\nMessage:\n${content.body}`
				}
				myEmailTransporter.sendMail(mailOptions, function(error, info) {
					if (error) {
						console.log(error);
					}
					else
					{
						console.log('email sent: ' + info.response);
					}
				});
				res.writeHead(200, { 'Content-Type': 'text/html'});
				fs.readFile('./index.html', fsCallback);
			}
			else
				res.end('ERROR: Please enter all required data.');
		})
	}
});

function contentCleaner(content)
{
	var tab = content.split('&');
	obj = {
		'firstName': tab[0].split('=')[1],
		'lastName': tab[1].split('=')[1],
		'email': tab[2].split('=')[1],
		'subject': tab[3].split('=')[1],
		'body': tab[4].split('=')[1].split('+').join(' ')
	}
	return (obj);
}

server.listen(port, hostname, () => {
	console.log(`Server running at http://${hostname}:${port}`);
});
