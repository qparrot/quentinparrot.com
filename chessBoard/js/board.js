// NOTE: this example uses the chess.js library:
// https://github.com/jhlywa/chess.js

var board1 = null;
var game = new Chess();

var calculateBestMove =function(game)
{
	var newGameMoves = game.moves();

	return newGameMoves[calculateBestMove(game)];

};
/* Start chess Algo */ 

var calculateBestMove = function(game) 
{
	var processedValue = 0;
	var bestValue = -9999;
	var currentMove = 0;
	var bestMove = 0;
	var i = 0;
	var j = 0;
	var gameMoves = game.moves();
	while (currentMove < gameMoves.length)
	{
		while( i < 8)
		{
			while(j < 8)
			{
				game.move(game.moves()[currentMove]);
				processedValue = processedValue + evaluateCaseValue(game.board()[i][j]);
				game.undo();
				j++;
			}
			j = 0;
			i++;
		}
		if (bestValue < - processedValue)
		{
			bestValue = - processedValue;
			bestMove = currentMove;
		}
		i = 0;
		processedValue = 0;
		currentMove++;
	}
	return gameMoves[bestMove];

};

function evaluateCaseValue(piece)
{
	if(!piece)
		return 0;
	else if(piece.type === 'p')
		return 10;
	else if(piece.type === 'n')
		return 30;
	else if(piece.type === 'b')
		return 30;
	else if(piece.type === 'r')
		return 50;
	else if(piece.type === 'q')
		return 90;
	else if(piece.type === 'k')
		return 900;
}

var makeBestMove = function () 
{
	var bestMove = getBestMove(game);
	game.move(bestMove);
	board1.position(game.fen());
	if (game.game_over())
	{
		alert('Game over');
	}
};

var getBestMove = function (game)
{
	if (game.game_over())
	{
		alert('Game over');
	}
	var bestMove = calculateBestMove(game);
	return bestMove;
};

/* End of algorithm */
function onDragStart (source, piece, position, orientation)
{
	// do not pick up pieces if the game is over
	if (game.game_over())
		return false;

	// only pick up pieces for the side to move
	if ((game.turn() === 'w' && piece.search(/^b/) !== -1) ||
		(game.turn() === 'b' && piece.search(/^w/) !== -1)) 
	{
		return false;
	}
}

function onDrop (source, target)
{
	// see if the move is legal
	var move = game.move({
		from: source,
		to: target,
		promotion: 'q' // NOTE: always promote to a queen for example simplicity
	});

	// illegal move
	if (move === null)
		return 'snapback'

	window.setTimeout(makeBestMove, 250);

	updateStatus();

}

// update the board position after the piece snap
// for castling, en passant, pawn promotion
function onSnapEnd ()
{
	board1.position(game.fen());
}

function updateStatus ()
{
	var status = '';

	var moveColor = 'White';
	if (game.turn() === 'b')
	{
		moveColor = 'Black';
	}

	// checkmate?
	if (game.in_checkmate())
	{
		status = 'Game over, ' + moveColor + ' is in checkmate.';
	}
	// draw?
	else if (game.in_draw())
	{
		status = 'Game over, drawn position';
	}
	// game still on
	else 
	{
		status = moveColor + ' to move';

		// check?
		if (game.in_check())
		{
			status += ', ' + moveColor + ' is in check';
		}
	}

}

var config = {
	draggable: true,
	position: 'start',
	onDragStart: onDragStart,
	onDrop: onDrop,
	onSnapEnd: onSnapEnd
};
board1 = Chessboard('board1', config);

updateStatus();
