# chessBoard

## GOAL

Develop a small algorithm against whom we could play.
The algorithm must eat every most valuable piece available to eat in one move.

## Librairies

- chessboard.js
	This allows me to display a proper chess board.
- chess.js
	This allows me to implement the rules of chess and make moves.

## Resource:

https://www.freecodecamp.org/news/simple-chess-ai-step-by-step-1d55a9266977/
